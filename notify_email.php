<?php
include "vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//require 'PHPMailer-master/src/Exception.php';
//require 'PHPMailer-master/src/PHPMailer.php';
//require 'PHPMailer-master/src/SMTP.php';

if (php_sapi_name() == "cli") {
    $application = empty($argv[1])?'':$argv[1];
    $envMode = empty($argv[2])?'localhost':$argv[2];
    $fileName = empty($argv[3])?'':$argv[3];
}else{
    $application = $_REQUEST['project'];
    $envMode = empty($_REQUEST['envMode'])?'localhost':$_REQUEST['envMode'];
    $fileName = empty($_REQUEST['file_name'])?'master':$_REQUEST['file_name'];
}

$fileLink = 'http://localhost/suvyadeployment/deploy_history/'.$fileName;

if ('prod' == $envMode){
    $fileLink = 'https://sp.isha.in/deploy_history/'.$fileName;  
}
if ('aws_uat' == $envMode){
    $fileLink = 'http://uat-suvya.isha.in/sadhanapadaportal/deploy_history/'.$fileName;  
}

$mail = new PHPMailer();
$mail->IsSMTP();
$mail->Mailer = "smtp";

$mail->SMTPDebug  = 0;
$mail->SMTPAuth   = TRUE;
$mail->SMTPSecure = "tls";
$mail->Port       = 587;
$mail->Host       = "smtp.gmail.com";
$mail->Username   = "suvya-debug@ishafoundation.org";
$mail->Password   = "dxzvecvibqyhqvqf";

$mail->IsHTML(true);
$mail->AddAddress("arun.natarajan@ishafoundation.org", "Arun N");
$mail->AddAddress("durga.jeyachandran@ishafoundation.org ", "Durga");
$mail->SetFrom("suvya-debug@ishafoundation.org", "Suvya Deployment");
// $mail->AddReplyTo("reply-to-email@domain", "reply-to-name");
// $mail->AddCC("cc-recipient-email@domain", "cc-recipient-name");
$mail->Subject = "Suvya Application Deployment";
$content = "<p>Namaskaram, <br>
  The Suvya {$application} is deployed in {$envMode}.<br><br>
  
  Please find the changes here <a href ='{$fileLink}' target='_blank'>{$fileLink}</a>

  <br><br>
  Pranam
</p>";

$mail->MsgHTML($content); 
if(!$mail->Send()) {
  echo "Error while sending Email.";
  var_dump($mail);
} else {
  echo "Email sent successfully";
}

