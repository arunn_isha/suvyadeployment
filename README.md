Suvya Deployment
==================

On new install of this project crate deploy_history folder into this directory 
and run composer install to bring php mailer

Usage In LocalHost
==================

<ul>
 <li>Update Repo Only:<br>
 php update_repo.php VCD localhost >> /var/www/html/suvyaDeployment/deploy_history/vcd_release.html 2>&1
 </li>
 
 <li>Update Repo and Notify Email:<br>
php update.php VCD localhost master vcd_release_fix.html 
 </li>

 <li>Templates:<br>
php update.php ProjectName envMode branchName responseFileName.html 
 </li>

</ul> 



ProjectName & Mode
==================
<ul>
 <li>ProjectName: VRO, VCD</li>
 <li>envMode: localhost, test or prod</li>
</ul> 