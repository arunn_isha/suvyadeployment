<?php

if (php_sapi_name() == "cli") {
    $project = $argv[1];
    $envMode = empty($argv[2])?'localhost':$argv[2];
    $updateBranch = empty($argv[3])?'master':$argv[3];
    $fileName = empty($argv[4])?'fileName':$argv[4];
}else{
    $project = $_REQUEST['project'];
    $envMode = empty($_REQUEST['envMode'])?'localhost':$_REQUEST['envMode'];
    $updateBranch = empty($_REQUEST['branch_name'])?'master':$_REQUEST['branch_name'];
    $fileName = empty($_REQUEST['fileName'])?'fileName':$_REQUEST['fileName'];
}


$outputLocation = '/var/www/html/suvyadeployment/deploy_history/'.$fileName;

if ('prod' == $envMode){
    $outputLocation = '/suvyasuite/suvya_sp/web/deploy_history/'.$fileName;
}
if ('aws_uat' == $envMode){
    $outputLocation = '/var/www/html/sadhanapadaportal/deploy_history/'.$fileName;
}

$a_comments = array(
    'php update_repo.php '.$project.' '.$envMode.' >> '.$outputLocation.'  2>&1',
    'php notify_email.php '.$project.' '.$envMode.' '.$fileName
);

$output = shell_exec(implode(" ;", $a_comments));
echo '<div class="output">';
echo "<pre>".$output."</pre>";
echo '</div>';
echo '</div>';