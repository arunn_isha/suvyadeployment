<?php
include "vendor/autoload.php";

if (php_sapi_name() == "cli") {
    $project = $argv[1];
    $envMode = empty($argv[2])?'localhost':$argv[2];
    $updateBranch = empty($argv[3])?'master':$argv[3];
}else{
    $project = $_REQUEST['project'];
    $envMode = empty($_REQUEST['envMode'])?'localhost':$_REQUEST['envMode'];
    $updateBranch = empty($_REQUEST['branch_name'])?'master':$_REQUEST['branch_name'];
}


if (empty($project)) {
    echo "<font color='red'>Project should not be empty</font>";
    exit;
}

$a_application_path = array (
    'prod' => array(
        'VRO' => '/suvyasuite/vro/',
        'VCD' => '/suvyasuite/vcd/',
        'GUEST' => '/suvyasuite/suvya/',
        'OCO' => '/suvyasuite/oco/',
        'RCD' => '/suvyasuite/rcd/',
        'ACRE' => '/suvyasuite/acre/',
        'CENTRAL' => '/suvyasuite/central/',
        'HR' => '/suvyasuite/hr/',
    ),
    'test' => array(
        'VRO' => '/var/www/html/suvyavro/',
        'VCD' => '/var/www/html/suvyavcd/',
        'GUEST' => '/var/www/html/suvyaguest/',
        'OCO' => '/var/www/html/suvyaoco/',
        'RCD' => '/var/www/html/suvyarcd/',
        'ACRE' => '/var/www/html/suvyaacre/',
        'CENTRAL' => '/var/www/html/suvyacentral/',
        'HR' => '/var/www/html/suvyahr/',
        
    ),
     'aws_uat' => array(
        'VRO' => '/var/www/html/suvyavro/',
        'VCD' => '/var/www/html/suvyavcd/',
        'GUEST' => '/var/www/html/suvyaguest/',
        'OCO' => '/var/www/html/suvyaoco/',
        'RCD' => '/var/www/html/suvyarcd/',
        'ACRE' => '/var/www/html/suvyaacre/',
        'CENTRAL' => '/var/www/html/suvyacentral/',
        'HR' => '/var/www/html/suvyahr/',
        
    ),
    'localhost' => array(
        'VRO' => '/var/www/html/suvyavro/',
        'VCD' => '/var/www/html/suvyavcd/',
        'GUEST' => '/var/www/html/suvyaguest/',
        'OCO' => '/var/www/html/suvyaoco/',
        'RCD' => '/var/www/html/suvyarcd/',
        'ACRE' => '/var/www/html/suvyaacre/',
        'CENTRAL' => '/var/www/html/suvyacentral/',
        'HR' => '/var/www/html/suvyahr/',
    )
);

$a_repo = array (
    'Accommodation Bundle' => 'vendor/isha/accommodation-bundle/Isha/AccommodationBundleV2',
    'Admin Bundle' => 'vendor/isha/admin-bundle/Isha/AdminBundle',
    'AnalyticsBundle' => 'vendor/isha/analytics-bundle/Isha/AnalyticsBundle',
    'APIBundle' => 'vendor/isha/api-bundle/Isha/APIBundle',
    'BreadcrumbsBundle' => 'vendor/isha/breadcrumbs-bundle/Isha/BreadcrumbsBundle',
    'CalendarBundle' => 'vendor/isha/calendar-bundle/Isha/CalendarBundle',
    'FinanceBundle' => 'vendor/isha/finance-bundle/Isha/FinanceBundle',
    'FormBundle' => 'vendor/isha/form-bundle/Isha/FormBundle',
    'GuestBundle' => 'vendor/isha/guest-bundle/Isha/GuestBundle',
    'LogBundle' => 'vendor/isha/log-bundle/Isha/LogBundle',
    'MessageBundle' => 'vendor/isha/message-bundle/Isha/MessageBundle',
    'OCOBridgeBundle' => 'vendor/isha/oco-bridge-bundle/Isha/OCOBridgeBundle',
    'RCDBundle' => 'vendor/isha/rcd-bundle/Isha/RCDBundle',
    'SwiftmailerLoggerBundle' => 'vendor/isha/swiftmailerlogger-bundle/Isha/SwiftmailerLoggerBundle',
    'UIBundle' => 'vendor/isha/ui-bundle/Isha/UIBundle',
    'UserBundle' => 'vendor/isha/user-bundle/Isha/UserBundle',
    'UtilBundle' => 'vendor/isha/util-bundle/Isha/UtilBundle',
    'Yoga Center Bundle' => 'vendor/isha/yogacenter-bundle/Isha/YogaCenterBundle',
    'HRBundle' => 'vendor/isha/hr-bundle/Isha/HRBundle',
);

$codeBasePath = $a_application_path[$envMode][$project];

$currentDate = date("d-M-Y H:i A");

$deploymentTitle = $project . "(". $envMode ." - ". $updateBranch .") - on ". $currentDate;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Suvya Deployment <?php echo $deploymentTitle;?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <style>
    .update_section{
        margin-bottom: 15px;
        border: 1px solid #CCC;
    }
    .repo_head{
        padding:8px;
        font-weight: bold;
    }

    .update_section .output{
        padding: 10px;
     }
  </style>
</head>
<body>
<main id="main">
<section id="about">
  <div class="container">

  <center><h2><i><u>Deployment Started for <?php echo $deploymentTitle;?></u><i></h2></center>
  <br>
  <div class="update_section">
    <div class="repo_head bg-danger text-white">Project Details</div>
    <div class="output">
       <ul>
          <li>Project: <?php echo $project;?></li>
          <li>Environment: <?php echo $envMode;?></li>
          <li>Code Base Path: <?php echo $codeBasePath;?></li>
       </ul>
    </div>
  </div>

<?php
try
{
    echo '<div class="update_section">';
    echo '<div class="repo_head bg-info text-white">Updating Main Projects</div>';
    $a_comments = array(
        'cd '.$codeBasePath,
        'git stash',
        'git checkout master',
        'git pull origin master',
        //'ls -la'
    );

    if ('master' != $updateBranch) {
        $a_comments = array(
            'cd '.$codeBasePath,
            'git stash',
            'git checkout -b '.$updateBranch,
            'git pull origin '.$updateBranch,
            //'ls -la'
        );      
    }

    if ('current' == $updateBranch) {
        $a_comments = array(
            'cd '.$codeBasePath,
            'git stash',
            'git pull',
            //'ls -la'
        );      
    }

    $output = shell_exec(implode(" ;", $a_comments));
    echo '<div class="output">';
    echo "<pre>".$output."</pre>";
    echo '</div>';
    echo '</div>';

}
catch (\Exception $e)
{
    echo "<center><font color='red'>Fail to update main Project <br>". $e->getMessage()."<br>";
    exit;
}



//-- update vendor repo
try
{
    foreach ($a_repo as $bundle => $repoDir) {

        $repoDir = $codeBasePath.$repoDir;

        if (is_dir($repoDir)) {
            echo '<div class="update_section">';
            echo '<div class="repo_head bg-info text-white">Updating '.$bundle.'</div>';
            echo "<br>";

            $a_comments = array(
                'cd '.$repoDir,
                'git stash',
                'git checkout master',
                'git pull origin master',
                //'ls -la'
            );

            if ('master' != $updateBranch) {
                $a_comments = array(
                    'cd '.$codeBasePath,
                    'git stash',
                    'git checkout -b '.$updateBranch,
                    'git pull origin '.$updateBranch,
                    //'ls -la'
                );      
            }


            if ('current' == $updateBranch) {
                $a_comments = array(
                    'cd '.$codeBasePath,
                    'git stash',
                    'git pull',
                    //'ls -la'
                );      
            }            

            $output = shell_exec(implode(" ;", $a_comments));
            echo '<div class="output">';
            echo "<pre>".$output."</pre>";
            echo '</div>';
            echo '</div>';
        }
    }
}
catch (\Exception $e)
{
    echo "<center><font color='red'>Fail to update repo <br>". $e->getMessage()."<br>";
    exit;
}


// if ('localhost' == $envMode){
//     echo "Done";
//     exit;
// }

//-- config the respective config files
try
{
    if ('prod' == $envMode){
        $a_comments = array(
            'cd '.$codeBasePath,
            'cp app/config/production_server/config_prod.yml.dist app/config/config_prod.yml',
            'cp app/config/production_server/parameters.yml.dist app/config/parameters.yml',
            'cp app/config/production_server/security.yml.dist app/config/security.yml',
            'cat app/config/parameters.yml'
        );
    }else if('test' == $envMode) {
        $a_comments = array(
            'cd '.$codeBasePath,
            'cp app/config/test_server/config_prod.yml.dist app/config/config_prod.yml',
            'cp app/config/test_server/parameters.yml.dist app/config/parameters.yml',
            'cp app/config/test_server/security.yml.dist app/config/security.yml',
            'cat app/config/parameters.yml'
        );     
    }else if('aws_uat' == $envMode) {
        $a_comments = array(
            'cd '.$codeBasePath,
            'cp app/config/aws_uat_server/config_prod.yml.dist app/config/config_prod.yml',
            'cp app/config/aws_uat_server/parameters.yml.dist app/config/parameters.yml',
            'cp app/config/aws_uat_server/security.yml.dist app/config/security.yml',
            'cat app/config/parameters.yml'
        );     
    }
    else {
        $a_comments = array(
            'cd '.$codeBasePath,
            'cp app/config/local_host/config_prod.yml.dist app/config/config_prod.yml',
            'cp app/config/local_host/parameters.yml.dist app/config/parameters.yml',
            'cp app/config/local_host/security.yml.dist app/config/security.yml',
            'cat app/config/parameters.yml'
        );      
    }
   
    $output = shell_exec(implode(" ;", $a_comments));
}
catch (\Exception $e)
{
    $output = "<center><font color='red'>Fail to copy files <br>". $e->getMessage();
}
?>
 <div class="update_section">
    <div class="repo_head bg-danger text-white">Copying Config Files</div>
    <div class="output">
       <pre><?php echo $output;?>
    </div>
  </div>

<?php
// Database change
try
{
    $a_comments = array(
        'cd '.$codeBasePath,
        'chmod -R 777 var/cache/',
        'chmod -R 777 var/cache/*',
        'php bin/console doctrine:schema:update --dump-sql  --em=suvya',
    );

    $output = shell_exec(implode(" ;", $a_comments));
}
catch (\Expection $e)
{
    $output = "<center><font color='red'>Fail to generate DB SQL <br>". $e->getMessage(); 
}
?>
  <div class="update_section">
    <div class="repo_head bg-danger text-white">Database Changes</div>
    <div class="output">
       <pre><?php echo $output;?>
    </div>
  </div>

</div>
</section>
</body>
</html>